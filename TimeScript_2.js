function SetTimer(){
    // var selector = document.getElementsByid('formOpt');
    // var value = selector[selector.selectedIndex].value;
    document.getElementById("secs").value = 0;
    document.getElementById("mins").value = 0;
    document.getElementById("hrs").value=0;
    var selectBox = document.getElementById("formOpt");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    var calcMin;
    var calcSec;
    //alert(selectedValue);
    if(selectedValue>= 60){
        //alert("1");
        calcMin = selectedValue/60;
        calcSec = selectedValue%60;
        //alert(calcMin);
        //alert(calcSec);
        
        document.getElementById("secs").value = calcSec;
        document.getElementById("mins").value = calcMin;
        //document.getElementById("secs").disabled= true;
    }
    else if(selectedValue == 0){
        //alert("2");
        document.getElementById("secs").disabled= false;
        document.getElementById("mins").disabled=false;
        document.getElementById("hrs").disabled=false;
    }
    else{
        //alert("3");
        document.getElementById("secs").value = selectedValue;
        //document.getElementById("secs").disabled= true;
    }

    //document.getElementById("secs").value = selectedValue;
    //alert("4");
}
// 10 minutes from now

var time_in_minutes;
var time_in_seconds;
var current_time ;
var deadline;
var clockSec;
var clockMin;

function time_remaining(endtime){
	var t = Date.parse(endtime) - Date.parse(new Date());
	var seconds = Math.floor( (t/1000) % 60 );
	var minutes = Math.floor( (t/1000/60) % 60 );
	var hours = Math.floor( (t/(1000*60*60)) % 24 );
	var days = Math.floor( t/(1000*60*60*24) );
	return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
}

var timeinterval;
var t;
function run_clock(id,endtime){
    var clock = document.getElementById(id);
    clockSec = document.getElementById("secId");
    clockMin = document.getElementById("minId");
	function update_clock(){
		t = time_remaining(endtime);
        //clock.innerHTML = 'minutes: '+t.minutes+'<br>seconds: '+t.seconds;
        clockMin.innerHTML = ('0' + t.minutes).slice(-2);//t.minutes; 
        clockSec.innerHTML = ('0' + t.seconds).slice(-2); //t.seconds; 
        
        //clock.querySelector('.minutes').innerHTML = t.minutes;
        if(t.total<=0){ clearInterval(timeinterval);
            var x = document.getElementById("StopAudio"); 
            document.getElementById("secs").value = 0;
            document.getElementById("mins").value = 0;
            document.getElementById("hrs").value=0;
        
            x.play();
        }
	}
	update_clock(); // run function once at first to avoid delay
	timeinterval = setInterval(update_clock,1000);
}
function Testtt(){
    //alert("1");
    //document.getElementById("ok").setAttribute('disabled', true);
    time_in_hrs = document.getElementById("hrs").value;
    time_in_minutes = document.getElementById("mins").value; //10;
    time_in_seconds = document.getElementById("secs").value;
    
    
    current_time = Date.parse(new Date());
    deadline = new Date(current_time + time_in_hrs*60*60*1000 + time_in_minutes*60*1000 + time_in_seconds*1000);
    //alert(time_in_minutes);
    //alert(current_time);
    //alert(deadline);
    run_clock('clockdiv1',deadline);
    
}

var paused = false; // is the clock paused?
var time_left; // time left on the clock when paused

function pause_clock(){
	if(!paused){
		paused = true;
		clearInterval(timeinterval); // stop the clock
		time_left = time_remaining(deadline).total; // preserve remaining time
	}
}

function resume_clock(){
	if(paused){
		paused = false;

		// update the deadline to preserve the amount of time remaining
		deadline = new Date(Date.parse(new Date()) + time_left);

		// start the clock
		run_clock('clockdiv1',deadline);
	}
}

function stop_clock(){


    
        clearInterval(timeinterval); // stop the clock
        document.getElementById("secs").value =  0;
        document.getElementById("mins").value = 0;
        document.getElementById("hr").value = 0;
        clockMin.innerHTML = ('0' + '0').slice(-2);
        clockSec.innerHTML = ('0' + '0').slice(-2);
        //var audio = new Audio('SOUND108.WAV');
        
        
		
}

// handle pause and resume button clicks
document.getElementById('pause').onclick = pause_clock;
document.getElementById('resume').onclick = resume_clock;
document.getElementById('stop').onclick = stop_clock;
//---------------------------------------------------------------------------------//



// function getTimeRemaining(endtime) {
//     var t = Date.parse(endtime) - Date.parse(new Date());
//     var seconds = Math.floor((t / 1000) % 60);
//     var minutes = Math.floor((t / 1000 / 60) % 60);
//     var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
//     var days = Math.floor(t / (1000 * 60 * 60 * 24));
//     return {
//       'total': t,
//       'days': days,
//       'hours': hours,
//       'minutes': minutes,
//       'seconds': seconds
//     };
//   }
  
//   function initializeClock(id, endtime) {
//     var clock = document.getElementById(id);
//     var daysSpan = clock.querySelector('.days');
//     var hoursSpan = clock.querySelector('.hours');
//     var minutesSpan = clock.querySelector('.minutes');
//     var secondsSpan = clock.querySelector('.seconds');
  
//     function updateClock() {
//       var t = getTimeRemaining(endtime);
  
//       daysSpan.innerHTML = t.days;
//       hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
//       minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
//       secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
  
//       if (t.total <= 0) {
//         clearInterval(timeinterval);
//       }
//     }
  
//     updateClock();
//     var timeinterval = setInterval(updateClock, 1000);
//   }
  
//   var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
//   initializeClock('clockdiv', deadline);